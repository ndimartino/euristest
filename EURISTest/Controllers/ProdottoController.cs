﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EURIS.Service;
using EURIS.Entities;

namespace EURISTest.Controllers
{
    public class ProdottoController : Controller
    {
        private readonly ProdottoService _prod = new ProdottoService();

        public ActionResult Index()
        {
            List<Prodotto> prodotti = _prod.GetProdotti();
            ViewBag.Prodotti = prodotti;

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Prodotto prodotto)
        {
            if(ModelState.IsValid)
            {
                _prod.AddNewProdotto(prodotto);
                _prod.Save();
                return RedirectToAction("Index", "Prodotto");
            }

            return View(prodotto);
        }

        public ActionResult Details(int id)
        {
            var prodotto = _prod.GetProdottoById(id);
            if (prodotto == null)
            {
                return HttpNotFound();
            }
            return View(prodotto);
        }

        public ActionResult Edit(int id)
        {
            var prodotto = _prod.GetProdottoById(id);
            if (prodotto == null)
            {
                return HttpNotFound();
            }
            return View(prodotto);
        }

        [HttpPost]
        public ActionResult Edit(Prodotto prodotto)
        {
            if (ModelState.IsValid)
            {
                _prod.UpdateProdotto(prodotto);
                _prod.Save();
                return RedirectToAction("Edit", "Prodotto", new { id = prodotto.Id });
            }
            return View(prodotto);
        }

        public ActionResult Delete(int id = 0)
        {
            var prodotto = _prod.GetProdottoById(id);
            if (prodotto == null)
            {
                return HttpNotFound();
            }
            return View(prodotto);
        }
        
        [HttpPost,ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _prod.DeleteProdotto(id);
            _prod.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _prod.Dispose();
            base.Dispose(disposing);
        }
    }
}
