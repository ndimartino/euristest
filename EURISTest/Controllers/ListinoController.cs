﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EURIS.Service;
using EURIS.Entities;

namespace EURISTest.Controllers
{
    public class ListinoController : Controller
    {
        private readonly ListinoService _list = new ListinoService();
        private readonly ProdottoService _prod = new ProdottoService();

        public ActionResult Index()
        {
            List<Listino> listini = _list.GetListini();
            ViewBag.Listini = listini;

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Listino listino)
        {
            if (ModelState.IsValid)
            {
                _list.AddNewListino(listino);
                _list.Save();
                return RedirectToAction("Index", "Listino", new { id = listino.Id });
            }

            return View(listino);
        }

        public ActionResult Details(int id)
        {
            var listino = _list.GetListinoById(id);
            if (listino == null)
            {
                return HttpNotFound();
            }
            return View(listino);
        }

        public ActionResult Edit(int id)
        {
            var listino = _list.GetListinoById(id);
            if (listino == null)
            {
                return HttpNotFound();
            }
            return View(listino);
        }

        [HttpPost]
        public ActionResult Edit(Listino listino)
        {
            if (ModelState.IsValid)
            {
                _list.UpdateListino(listino);
                _list.Save();
                return RedirectToAction("Edit", "Listino", new { id = listino.Id });
            }
            return View(listino);
        }

        public ActionResult Delete(int id = 0)
        {
            var listino = _list.GetListinoById(id);
            if (listino == null)
            {
                return HttpNotFound();
            }
            return View(listino);
        }


        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _list.DeleteListino(id);
            _list.Save();
            return RedirectToAction("Index");
        }

        public ActionResult AddProdotti(int id)
        {
            var listino = _list.GetListinoById(id);
            var prodotti = _prod.GetProdotti();

            ViewBag.Prodotti = prodotti;

            if (listino == null)
            {
                return HttpNotFound();
            }
            return View(listino);
        }

        [HttpPost]
        public ActionResult AddProdotti(int idListino, List<Prodotto> prodotti)
        {
            foreach(Prodotto p in prodotti)
            {
                _list.AddProdottoToListino(idListino, p);
            }
            _list.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _list.Dispose();
            base.Dispose(disposing);
        }
    }
}
