﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EURIS.Entities;
using System.Data.Entity;
using System.Data;

namespace EURIS.Service
{
    public class ProdottoService
    {
        private readonly LocalDbEntities _context = new LocalDbEntities();

        //GET ALL PRODOTTI
        public List<Prodotto> GetProdotti()
        {
            List<Prodotto> prodotti = new List<Prodotto>();

            prodotti = (from item in _context.ProdottoSet
                        select item).ToList();

            return prodotti;
        }

        //GET SINGLE PRODOTTO
        public Prodotto GetProdottoById(int id)
        {
            return _context.ProdottoSet.Find(id);
        }

        //ADD PRODOTTO
        public void AddNewProdotto(Prodotto prodotto)
        {
            _context.ProdottoSet.Add(prodotto);
        }
        
        //UPDATE PRODOTTO
        public void UpdateProdotto(Prodotto prodotto)
        {
            _context.Entry(prodotto).State = EntityState.Modified;

        }

        //DELETE PRODOTTO
        public void DeleteProdotto(int id)
        {
            var prodotto = _context.ProdottoSet.Find(id);
            _context.ProdottoSet.Remove(prodotto);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
