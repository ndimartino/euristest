﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EURIS.Entities;
using System.Data.Entity;
using System.Data;

namespace EURIS.Service
{
    public class ListinoService
    {
        private readonly LocalDbEntities _context = new LocalDbEntities();

        //GET ALL LISTINI
        public List<Listino> GetListini()
        {
            List<Listino> listini = new List<Listino>();

            listini = (from item in _context.ListinoSet
                        select item).ToList();

            return listini;
        }

        //GET SINGLE LISTINO
        public Listino GetListinoById(int id)
        {
            return _context.ListinoSet.Find(id);
        }

        //ADD LISTINO
        public void AddNewListino(Listino listino)
        {
            _context.ListinoSet.Add(listino);
        }

        //UPDATE LISTINO
        public void UpdateListino(Listino listino)
        {
            _context.Entry(listino).State = EntityState.Modified;
        }

        //DELETE LISTINO
        public void DeleteListino(int id)
        {
            var listino = _context.ListinoSet.Find(id);
            _context.ListinoSet.Remove(listino);
        }

        //ADD PRODOTTO TO LISTINO
        public void AddProdottoToListino(int idListino, Prodotto prodotto)
        {
            var listino = _context.ListinoSet.Find(idListino);
            listino.ProdottoSet.Add(prodotto);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
